-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2017 at 09:26 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `capitol2`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(3) NOT NULL,
  `cat_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_title`) VALUES
(1, 'tuguegarao'),
(2, 'solana'),
(3, 'enrile'),
(4, 'iguig'),
(15, 'gattaran'),
(16, 'tuao'),
(17, 'dddd'),
(18, 'dddd'),
(19, 'ddww'),
(20, 'dddww'),
(21, 'yyy'),
(22, 'kkk');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(3) NOT NULL,
  `post_category_id` varchar(255) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_author` varchar(255) NOT NULL,
  `post_date` date NOT NULL,
  `post_image` text NOT NULL,
  `post_content` text NOT NULL,
  `post_tags` varchar(255) NOT NULL,
  `post_comment_count` varchar(255) NOT NULL,
  `post_status` varchar(255) NOT NULL DEFAULT 'draft',
  `post_municipalities` varchar(250) NOT NULL,
  `Status` varchar(50) NOT NULL,
  `start-date` text NOT NULL,
  `end-date` text NOT NULL,
  `notification` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `post_category_id`, `post_title`, `post_author`, `post_date`, `post_image`, `post_content`, `post_tags`, `post_comment_count`, `post_status`, `post_municipalities`, `Status`, `start-date`, `end-date`, `notification`) VALUES
(67, '', 'qwer', 'bffdgdf', '2017-11-27', '2016-03-03 14.39.37.jpg', '<p>fgfdgdfgdf</p>', '', '4', 'draft', 'enrile', 'Approve', '1111-11-11', '2222-02-22', ''),
(68, '', 'qwer', 'qwerty', '2017-11-27', '2016-03-03 14.39.37.jpg', '<p>vdferfer</p>', '', '4', 'draft', 'solana', 'Approve', '1111-11-11', '2222-02-22', '');

-- --------------------------------------------------------

--
-- Table structure for table `tblmunicipalities`
--

CREATE TABLE `tblmunicipalities` (
  `userid` int(11) NOT NULL,
  `municipalities` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblmunicipalities`
--

INSERT INTO `tblmunicipalities` (`userid`, `municipalities`, `username`, `password`, `img`) VALUES
(1, 'tuguegarao', 'tuguegarao', 'tuguegarao', ''),
(2, 'enrile', 'enrile', 'enrile', ''),
(3, 'solana', 'solana', 'solana', ''),
(4, 'gattaran', 'gattaran', 'gattaran', ''),
(5, 'alcala', 'alcala', 'alcala', ''),
(8, 'lallo', 'lallo', 'lallo', '10-dithering-opt.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_update`
--

CREATE TABLE `tbl_update` (
  `id` int(11) NOT NULL,
  `post_id` text NOT NULL,
  `description` text NOT NULL,
  `date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_update`
--

INSERT INTO `tbl_update` (`id`, `post_id`, `description`, `date`) VALUES
(1, '61', 'vsdfsd', '2017-12-30'),
(2, '67', 'ttrgertgerg', '1111-11-11');

-- --------------------------------------------------------

--
-- Table structure for table `tuguegarao`
--

CREATE TABLE `tuguegarao` (
  `cid` int(11) NOT NULL,
  `uid` varchar(128) NOT NULL,
  `date` datetime NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuguegarao`
--

INSERT INTO `tuguegarao` (`cid`, `uid`, `date`, `message`) VALUES
(4, 'Tuguegarao City', '2017-10-09 21:52:50', 'proposal 1'),
(5, 'Tuguegarao City', '2017-10-10 06:46:18', 'awdwdws'),
(6, 'Tuguegarao City', '2017-10-28 14:38:59', 'wqdqwdqwd');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(3) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_firstname` varchar(255) NOT NULL,
  `user_lastname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_image` text NOT NULL,
  `user_role` varchar(255) NOT NULL,
  `something` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `user_password`, `user_firstname`, `user_lastname`, `user_email`, `user_image`, `user_role`, `something`) VALUES
(1, 'admin', 'admin', 'admin firstname', 'admin lastname', 'admin@admin.com', '', 'ADMIN', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `tblmunicipalities`
--
ALTER TABLE `tblmunicipalities`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `tbl_update`
--
ALTER TABLE `tbl_update`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuguegarao`
--
ALTER TABLE `tuguegarao`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `tblmunicipalities`
--
ALTER TABLE `tblmunicipalities`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_update`
--
ALTER TABLE `tbl_update`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tuguegarao`
--
ALTER TABLE `tuguegarao`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
